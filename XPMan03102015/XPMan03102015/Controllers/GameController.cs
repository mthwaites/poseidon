﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace XPMan03102015.Controllers
{
	using System.IO;
	using System.Linq;
	using System.Web.UI.WebControls;

	using Newtonsoft.Json;
	using classes;

	using WebGrease.Css.Extensions;
	using System.Web.Hosting;

	using Microsoft.Ajax.Utilities;

	public class GridReference
	{
		public int Horizontal { get; set; }
		public int Vertical { get; set; }
		public string Reference { get; set; }

		public GridReference(string reference)
		{
			Reference = reference;
			Horizontal = GameController.LetterToNumber(reference.Substring(0, 1));
			Vertical = Convert.ToInt16(reference.Substring(1, reference.Length - 1));
		}

		public GridReference(int horizontal, int vertical)
		{
			if (horizontal == 0 || vertical == 0)
				return;

			Horizontal = horizontal;
			Vertical = vertical;
			Reference = GameController.IndexToColumn(horizontal) + Vertical.ToString();
		}
	}

	public class GameController : Controller
	{
		private static Dictionary<string, ShipType> _shipsMines = new Dictionary<string, ShipType>();
		private static Dictionary<string, ResultType> _moves = new Dictionary<string, ResultType>();
		private static List<string> _hits = new List<string>();
		private static List<GridReference> _currentTarget = new List<GridReference>();
        private static Stack<string> _minePreferences = new Stack<string>(); 

		private static Start _start;
		private static int _gridHorizontal;
		private static int _gridVertical;
		private static int _minesRemaining;
        private static int _maximumMines = 100;

	    private static StreamWriter _logging = new StreamWriter(new FileStream(HostingEnvironment.MapPath("~/App_Data/dummy.log"), FileMode.Append));

		protected override void OnActionExecuted(ActionExecutedContext filterContext)
		{
			_logging.Flush();
			base.OnActionExecuted(filterContext);
		}

		protected override void OnException(ExceptionContext filterContext)
		{
			_logging.WriteLine(filterContext.Exception.ToString());

			base.OnException(filterContext);
		}

		[HttpPost]
		public ActionResult START(Start start)
		{
		    var playerNames = "";
		    foreach (var player in start.players)
		    {
		        playerNames += player + "-";
		    }
            _logging = new StreamWriter(new FileStream(HostingEnvironment.MapPath("~/App_Data/" + playerNames+DateTime.Now.Ticks + ".log"), FileMode.Append));

		    _maximumMines = 5;
			_moves = new Dictionary<string, ResultType>();
			_shipsMines = new Dictionary<string, ShipType>();
			_start = start;
			_gridHorizontal = LetterToNumber(_start.gridSize.Substring(0, 1));
			_gridVertical = Convert.ToInt16(_start.gridSize.Substring(1, _start.gridSize.Length - 1));
			return Json("OK");
		}

		[HttpGet]
		public ActionResult MOVE()
		{
			if (_start.mineCount > 0 && _maximumMines > 0)
			{
				_start.mineCount--;
			    _maximumMines--;
				return PlaceMine();
			}
			return Json(new { type = "Attack", gridReference = GetReference() }, JsonRequestBehavior.AllowGet);

		}

		private ActionResult PlaceMine()
		{
		    while (_minePreferences.Count > 0)
		    {
		        var mineLocation = _minePreferences.Pop();
		        if (!_shipsMines.ContainsKey(mineLocation))
		        {
                    _logging.WriteLine("Mine - " + mineLocation);
					_shipsMines.Add(mineLocation, ShipType.Mine);
		            return Json(new { Type = "MINE", gridReference = mineLocation}, JsonRequestBehavior.AllowGet);
		        }
		    }

		    while (true)
			{
				var rand = new Random();
				var letter = rand.Next(1, _gridHorizontal + 1);
				var number = rand.Next(1, _gridVertical + 1);
				var reference = string.Format("{0}{1}", IndexToColumn(letter), number);
				if (_shipsMines.ContainsKey(reference))
					continue;

                _logging.WriteLine("Mine - " + reference);
                return Json(new { type = "MINE", gridReference = reference }, JsonRequestBehavior.AllowGet);

			}
		}

		public string GetReference()
		{
			if (_currentTarget.Count > 0)
			{
				return FindShip();
			}
			return GetRandomPlace();
		}

		public string GetRandomPlace()
		{
			while (true)
			{
				var rand = new Random();

				var letter = rand.Next(1, _gridHorizontal + 1);
				var number = rand.Next(1, _gridVertical + 1);
				var reference = string.Format("{0}{1}", IndexToColumn(letter), number);
				if (_moves.ContainsKey(reference))
					continue;

				return reference;
			}
		}

        public string GetOrderedPlace()
        {
            return "";
        }

		public string FindShip()
		{
			_logging.WriteLine("Found Ship {0}", _currentTarget.Count);

			if (_currentTarget.Count > 1)
			{
				var first = _currentTarget[0];
				var next = _currentTarget[1];
				string response;
				if (next.Horizontal == first.Horizontal)
				{
					response = FindHorizontal();
				}

				response = FindVertical();

				if (response == string.Empty)
				{
					_currentTarget.Clear();
					return GetRandomPlace();
				}
			}
			else
			{
				string response = string.Empty;
				try { 
					response = FindHorizontal(false);
					_logging.WriteLine("Find Horizontal {0}", response);

					if (response == string.Empty)
					{ 
						response = FindVertical(false);
						_logging.WriteLine("Find Vertical {0}", response);
					}
				}
				catch (Exception)
				{

				}

                if (response == string.Empty)
				{ 
					response = GetRandomPlace();
					_logging.WriteLine("Find Random {0}", response);
				}
				return response;
			}
		}

		public string FindHorizontal(bool useRandom = true)
		{
			var first = _currentTarget[0];
			var targetList = _currentTarget.OrderBy(t => t.Horizontal);
			var upperReference = new GridReference(targetList.Last().Horizontal + 1, first.Vertical);
			var lowerReference = new GridReference(targetList.First().Horizontal - 1, first.Vertical);
			if (targetList.Last().Horizontal <= _gridHorizontal && !_moves.ContainsKey(upperReference.Reference))
				return upperReference.Reference;
			else if (lowerReference.Horizontal != 0 && targetList.First().Horizontal == 0 && !_moves.ContainsKey(lowerReference.Reference))
				return lowerReference.Reference;

			return string.Empty;
		}

		public string FindVertical(bool useRandom = true)
		{
			var first = _currentTarget[0];
			var targetList = _currentTarget.OrderBy(t => t.Vertical);
			var upperReference = new GridReference(first.Horizontal, targetList.Last().Vertical + 1);
			var lowerReference = new GridReference(first.Horizontal, targetList.First().Vertical - 1);
			if (targetList.Last().Vertical <= _gridVertical && !_moves.ContainsKey(upperReference.Reference))
				return upperReference.Reference;
			else if (lowerReference.Vertical != 0 && targetList.First().Vertical == 0 && !_moves.ContainsKey(lowerReference.Reference))
				return lowerReference.Reference;


			return string.Empty;
		}

		public static int LetterToNumber(string letter)
		{
			return char.ToUpper(letter.ToCharArray()[0]) - 64;
        }

		static readonly string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH" };
		public static string IndexToColumn(int index)
		{
			if (index <= 0)
				throw new IndexOutOfRangeException("index must be a positive number");
			return Columns[index - 1];
		}

		[HttpGet]
		public ActionResult PLACE()
		{
			return Json(string.Empty, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult PLACE(List<string> gridReferences)
		{
			_logging.WriteLine(string.Join(",", gridReferences));
			_logging.Flush();

            foreach (var reference in gridReferences)
			{
				_shipsMines.Add(reference, ShipType.Ship);
				try { 
					SetMinePreferences(reference);
				}
				catch (Exception)
				{

				}
            }

		    return Json("OK");
		}

        private static void SetMinePreferences(string first)
        {
            var gridHorizontal = LetterToNumber(first.Substring(0, 1));
            var gridVertical = Convert.ToInt16(first.Substring(1, first.Length - 1));
            if (gridHorizontal - 1 >= 1)
            {
                string item = Columns[gridHorizontal - 1] + gridVertical;
                if (!_minePreferences.Contains(item))
                {
                    _minePreferences.Push(item);
                }
            }
            if (gridHorizontal + 1 <= _gridHorizontal)
            {
                string item = Columns[gridHorizontal + 1] + gridVertical;
                if (!_minePreferences.Contains(item))
                {
                    _minePreferences.Push(item);
                }
            }
            if (gridVertical - 1 >= 1)
            {
                string item = Columns[gridHorizontal] + (gridVertical - 1);
                if (!_minePreferences.Contains(item))
                {
                    _minePreferences.Push(item);
                }
            }
            if (gridVertical + 1 <= _gridVertical)
            {
                string item = Columns[gridHorizontal] + (gridVertical + 1);
                if (!_minePreferences.Contains(item))
                {
                    _minePreferences.Push(item);
                }
            }
        }

		[HttpPost]
		public ActionResult HIT(MoveResult shipHit)
		{ 
            _logging.WriteLine("HIT:" + shipHit.Attacker + " - " + shipHit.GridReference);

            if (shipHit.Attacker.ToLower() == "poseidon")
			{
				_currentTarget.Add(new GridReference(shipHit.GridReference));
				_moves.Add(shipHit.GridReference, ResultType.Hit);
			}
			return Json(string.Empty);
		}

		[HttpPost]
		public ActionResult MISS(MoveResult shipMiss)
		{
            _logging.WriteLine("MISS:" + shipMiss.Attacker + " - " + shipMiss.GridReference);

			if (shipMiss.Attacker.ToLower() == "poseidon")
			{
				_moves.Add(shipMiss.GridReference, ResultType.Miss);
			}
			return Json(string.Empty);
		}

		[HttpPost]
		public ActionResult SCAN()
		{
			return Json(string.Empty);
		}

		[HttpPost]
		public ActionResult HIT_MINE(MoveResult mineHit)
		{
            _logging.WriteLine("HIT_MINE:" + mineHit.Attacker + " - " + mineHit.GridReference);

            if (mineHit.Attacker.ToLower() == "poseidon")
			{
				_moves.Add(mineHit.GridReference, ResultType.Mine);
			}
			return Json(string.Empty);
		}

		enum ResultType
		{
			Hit,
			FullShip,
			Miss,
			Mine
		}
		enum ShipType
		{
			Ship,
			Mine
		}
	}
}
