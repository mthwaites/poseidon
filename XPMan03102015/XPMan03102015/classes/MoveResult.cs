﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XPMan03102015.classes
{
	public class MoveResult
	{
		public string Attacker { get; set; }
		public string GridReference { get; set; }
	}
}