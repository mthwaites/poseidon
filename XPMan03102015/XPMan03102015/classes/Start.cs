﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XPMan03102015.classes
{
	using System.Web.Helpers;

	using Newtonsoft.Json;
	using System.ComponentModel;

	public class Start
	{

		public string gridSize { get; set; }

		public string[] players { get; set; }

		public int maxTurns { get; set; }

		public string[] ships { get; set; }

		[DisplayName("mineCount")]
		public int mineCount { get; set; }

	}
}